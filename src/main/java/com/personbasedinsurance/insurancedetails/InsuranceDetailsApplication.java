package com.personbasedinsurance.insurancedetails;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsuranceHolderAuthenticationApplication {

	public static void main(String[] args) {
		SpringApplication.run(InsuranceHolderAuthenticationApplication.class, args);
	}

}

