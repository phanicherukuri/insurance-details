package com.personbasedinsurance.insurancedetails.process;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.personbasedinsurance.insurancedetails.errorhandling.ErrorInfo;
import com.personbasedinsurance.insurancedetails.errorhandling.InvalidInputException;
import com.personbasedinsurance.insurancedetails.service.UserDetailsService;
import com.personbasedinsurance.insurancedetails.vo.UserDetails;

public class UserDetailsProcess {
	Logger logger = LoggerFactory.getLogger(UserDetailsProcess.class);

	@Autowired
	private UserDetailsService userDetailsService;
	
	public UserDetails getUserDetails(String insuranceNumber) {
		inputValidation(insuranceNumber);
		List<String> userDetailsList = userDetailsService.getUserDetails(insuranceNumber);
		return createOutput(userDetailsList);
	}

	private void inputValidation(String insuranceNumber) {
		loginValidation();
		insuranceNumberValidation(insuranceNumber);
	}

	private void loginValidation() {
		// to do
		// validate user authentication
	}

	private void insuranceNumberValidation(String insuranceNumber) {
		if (insuranceNumber != null && !insuranceNumber.isEmpty() && insuranceNumber.length() == 12) {
			String pattern = "^[A-Z0-9]*$";
			if (!insuranceNumber.matches(pattern)) {
				logger.debug(String.format("the user input, %1$s, doesn't only contain alphanumeric characters", insuranceNumber));
				List<ErrorInfo> errors = new ArrayList<>();
				ErrorInfo error = new ErrorInfo();
				error.setField(insuranceNumber);
				error.setMessage("please enter valid insurance number");
				errors.add(error);
				throw new InvalidInputException(errors);
			}
		} else {
			logger.debug("insurance number is missing");
			List<ErrorInfo> errors = new ArrayList<>();
			ErrorInfo error = new ErrorInfo();
			error.setField(insuranceNumber);
			error.setMessage("please enter valid insurance number");
			errors.add(error);
			throw new InvalidInputException(errors);
		}
	}
	
	private UserDetails createOutput(List<String> userDetails) {
		UserDetails output = new UserDetails();
		
		return output;
	}
}
