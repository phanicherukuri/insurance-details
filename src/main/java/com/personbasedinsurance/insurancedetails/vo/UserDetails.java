package com.personbasedinsurance.insurancedetails.vo;

public class UserDetails {

	private String name;
	private String dateOfBirth;
	private String gender;
	private String city;
	private String state;
	private String nationality;
	private String planName;
	private String paymentPerMonth;
	private String associatedCompany;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getPaymentPerMonth() {
		return paymentPerMonth;
	}
	public void setPaymentPerMonth(String paymentPerMonth) {
		this.paymentPerMonth = paymentPerMonth;
	}
	public String getAssociatedCompany() {
		return associatedCompany;
	}
	public void setAssociatedCompany(String associatedCompany) {
		this.associatedCompany = associatedCompany;
	}
	
	
}
