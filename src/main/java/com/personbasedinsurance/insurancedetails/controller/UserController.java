package com.personbasedinsurance.insurancedetails.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personbasedinsurance.insurancedetails.process.UserDetailsProcess;
import com.personbasedinsurance.insurancedetails.vo.UserDetails;

@RestController
@RequestMapping("/insurance")
public class UserController {

	Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	UserDetailsProcess getUserSpecificOptions;

	@GetMapping("/insuranceNumber")
	public UserDetails getUserDetails(@PathVariable String insuranceNumber) {
		// need to add authentication to check weather user is a logged in user or not
		return getUserSpecificOptions.getUserDetails(insuranceNumber);
	}

}
